## 数据集介绍

DuIE2.0是业界规模最大的中文关系抽取数据集，其schema在传统简单关系类型基础上添加了多元复杂关系类型，此外其构建语料来自百度百科、百度信息流及百度贴吧文本，全面覆盖书面化表达及口语化表达语料，能充分考察真实业务场景下的关系抽取能力。

## 数据预览

该任务的目标是对于给定的自然语言句子，根据预先定义的schema集合，抽取出所有满足schema约束的SPO三元组。schema定义了关系P以及其对应的主体S和客体O的类别。根据O值的复杂程度可以将目标关系划分为以下两种：

1.简单O值：  
也就是说O是一个单一的文本片段。例如，「妻子」关系的schema定义为：

> {S\_TYPE:人物,P:妻子,O\_TYPE:{@value:人物}}

简单O值是最常见的关系类型。为了保持格式统一，简单O值类型的schema定义也通过结构体保存，结构体中只有一个@value字段存放O值。

2.复杂O值：  
也就是说O是一个结构体，由多个语义明确的文本片段共同组成，多个文本片段对应了结构体中的多个槽位 (slot)。例如，「饰演」关系中O值有两个槽位@value和inWork，分别表示「饰演的角色是什么」以及「在哪部影视作品中发生的饰演关系」，其schema定义为：

> {S\_TYPE:娱乐人物,P:饰演,O\_TYPE:{@value:角色,inWork:影视作品}}

在复杂O值类型的定义中，@value槽位可以认为是该关系的默认O值槽位，对于该关系不可或缺，其他槽位均可缺省。

关系抽取输入/输出：

输入：一个或多个连续完整句子。

输出：句子中包含的所有符合给定schema约束的SPO三元组。

输入示例：

> { "text": "王雪纯是87版《红楼梦》中晴雯的配音者，她是《正大综艺》的主持人" }

输出示例：

> { "text": "王雪纯是87版《红楼梦》中晴雯的配音者，她是《正大综艺》的主持人", "spo\_list": \[ { "predicate": "配音", "subject": "王雪纯", "subject\_type": "娱乐人物", "object": { "@value": "晴雯", "inWork": "红楼梦" }, "object\_type": { "@value": "人物", "inWork": "影视作品" } }, { "predicate": "主持人", "subject": "正大综艺", "subject\_type": "电视综艺", "object": { "@value": "王雪纯" }, "object_type": { "@value": "人物" } } \] }

## 基线系统

本项目将提供基于飞桨框架PaddlePaddle2.0和PaddleNLP的开源基线系统，提供丰富的高层API，全面支持动态图开发模式，从开发、训练到预测部署提供优质的整体体验。推荐您参照基线方案，基于PaddleNLP的丰富API库进行二次开发、模型调优和方案创新。

**GitHub 基线系统**[LIC2021竞赛 DuIE 关系抽取基线](https://github.com/PaddlePaddle/PaddleNLP/tree/develop/examples/information_extraction/DuIE)

## 数据集引用

如在学术论文中使用千言数据集，请添加相关引用说明，具体如下：

Li S., et al. (2019) DuIE: A Large-Scale Chinese Dataset for Information Extraction. In: Tang J., Kan MY., Zhao D., Li S., Zan H. (eds.) Natural Language Processing and Chinese Computing. NLPCC 2019. Lecture Notes in Computer Science, vol 11839. Springer, Cham.[论文地址](https://doi.org/10.1007/978-3-030-32236-6_72)
